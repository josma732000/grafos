
package grafos;

public class Grafos {

    
    public static Graph getCities(){
        Nodo camp = new Nodo("Campeche");
        Nodo tena = new Nodo("Tenabo");
        Nodo can = new Nodo("Candelaria");
        Nodo cham = new Nodo("Champoton");
        Nodo cc = new Nodo("cuidad del carmen");
        Nodo esc = new Nodo("Escarcega");
        Nodo hec = new Nodo("Hecelchakan");
        Nodo hop = new Nodo("Hopelchen");
        

        
        
        camp.addEdge(new Edge(camp, tena, 43.6));
        camp.addEdge(new Edge(camp, can, 212.5));
        camp.addEdge(new Edge(camp, cham, 27));
        camp.addEdge(new Edge(camp, cc, 206));
        camp.addEdge(new Edge(camp, esc, 147));
        camp.addEdge(new Edge(camp, hec, 66));
        camp.addEdge(new Edge(camp, hop, 94));
 
        Graph graph = new Graph();
        graph.addNodo(camp);
        graph.addNodo(tena);
        graph.addNodo(can);
        graph.addNodo(cham);
        graph.addNodo(cc);
        graph.addNodo(esc);
        graph.addNodo(hec);
        graph.addNodo(hop);

        return graph;

 
    }
            
            
    public static void main(String[] args) {
        
        Graph graph = getCities();
        System.out.println(graph);
    }
    
}
