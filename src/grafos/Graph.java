/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jose
 */
public class Graph {
   private List<Nodo> nodos;

    public void addNodo(Nodo node) {
        if (nodos == null) {
            nodos = new ArrayList<>();
        }
        nodos.add(node);
    }
    
    public List<Nodo> getNodos() {
        return nodos;
    }

    @Override
    public String toString() {
        return "Graph{" + "nodos=" + nodos + '}';
    }

}
